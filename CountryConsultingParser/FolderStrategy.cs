﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CountryConsultingParser
{
    class FolderStrategy : MainForm, ISourceValidator
    {
        public bool ValidateSources()
        {
            //check if source and target paths are valid
            String sourcePath = txtSourcePath.Text;

            if (String.IsNullOrEmpty(sourcePath))
            {
                txtStatus.Text = Constants.NoSourcePath;
                return false;
            }

            if (!Directory.Exists(sourcePath))
            {
                txtStatus.Text = Constants.InvalidSourcePath;
                return false;
            }

            //check validity of source files
            if (Directory.GetFiles(sourcePath).Length == 0)
            {
                txtStatus.Text = Constants.NoFiles;
                return false;
            }

            foreach (String file in Directory.GetFiles(sourcePath))
            {
                if (Path.GetExtension(file) != Constants.FileExtension)
                {
                    txtStatus.Text = Constants.InvalidExtension;
                    return false;
                }
            }

            return true;
        }
    }
}
