﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace CountryConsultingParser
{
    public partial class MainForm : Form
    {

        public List<string> sourceFiles { get; private set; }
        private ISourceValidator sourceValidationStrategy;

        public MainForm()
        {
            sourceFiles = new List<string>();
            InitializeComponent();
        }

        /// <summary>
        /// Clickhandler for starting conversion. This method contains the main workflow of the formatting process
        /// </summary>
        private void btnRun_Click(object sender, EventArgs e)
        {
            String sourcePath = txtSourcePath.Text;
            String targetPath = txtTargetPath.Text;

            if (sourceFiles.Count == 0)
            {
                sourceValidationStrategy = new FolderStrategy();
            }
            else
            {
                sourceValidationStrategy = new FileStrategy();
            }

            if (!sourceValidationStrategy.ValidateSources()) return;

            if (String.IsNullOrEmpty(targetPath))
            {
                txtStatus.Text = Constants.NoTargetPath;
                return;
            }

            if (!Directory.Exists(targetPath))
            {
                txtStatus.Text = Constants.InvalidTargetPath;
                return;
            }

            //initialize GUI
            txtStatus.Text = "";
            prbProgress.Maximum = sourceFiles.Count;
            prbProgress.Step = 1;
            prbProgress.Value = 0;

            //start conversion
            String currentFile = "";
            foreach (String file in sourceFiles)
            {
                try
                {
                    currentFile = file;
                    //read the file
                    StreamReader sr = new StreamReader(file);
                    String fileContents = sr.ReadToEnd();
                    String[] liness;

                    //convert it
                    String[] lines = ConvertionHelper.SplitLines(fileContents);
                    lines = ConvertionHelper.SplitFieldsByTabs(lines);
                    lines = ConvertionHelper.CutHead(lines);
                    lines = ConvertionHelper.UpdateColumnNames(lines);
                    lines = ConvertionHelper.RemoveTotals(lines);
                    lines = ConvertionHelper.ReformatDate(lines);

                    //remove last newline
                    if (lines[lines.Length - 1] == "")
                    {
                        List<String> lineList = new List<String>(lines);
                        lineList.RemoveAt(lineList.Count - 1);
                        lines = lineList.ToArray();
                    }

                    //write the new file
                    String targetFile = targetPath + "\\" + Path.GetFileNameWithoutExtension(file) + ".txt";
                    File.Delete(targetPath + "\\" + Path.GetFileName(file));

                    StreamWriter sw = new StreamWriter(targetFile);
                    foreach (String line in lines)
                    {
                        sw.WriteLine(line);
                    }
                    sw.Close();
                    File.Move(targetFile, Path.ChangeExtension(targetFile, ".csv"));
                    prbProgress.PerformStep();
                }
                
                catch (Exception ex) //Todo: Create ConvertionException and make ConvertionHelper throw it
                {
                    txtStatus.Text += Environment.NewLine + "An error occured: " + currentFile;
                    continue;
                }
            }
        }

        /// <summary>
        /// Clickhandler for sourcepath BrowserDialog, opens a FolderBrowserDialog
        /// </summary>
        private void btnChooseSourcePath_Click(object sender, EventArgs e)
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.Multiselect = true;
                DialogResult result = ofd.ShowDialog();
                if (result == DialogResult.OK)
                {
                    txtSourcePath.Text = Convert.ToString(ofd.FileNames.Length) + " file(s) selected";
                    sourceFiles.AddRange(ofd.FileNames);
                }
            }
        }

        /// <summary>
        /// Clickhandler for targetpath BrowserDialog, opens a FolderBrowserDialog
        /// </summary>
        private void btnChooseTargetPath_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK)
                {
                    txtTargetPath.Text = fbd.SelectedPath;
                }
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }
    }
}
