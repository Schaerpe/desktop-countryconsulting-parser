﻿namespace CountryConsultingParser
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnChooseSourcePath = new System.Windows.Forms.Button();
            this.btnChooseTargetPath = new System.Windows.Forms.Button();
            this.txtSourcePath = new System.Windows.Forms.TextBox();
            this.txtTargetPath = new System.Windows.Forms.TextBox();
            this.btnRun = new System.Windows.Forms.Button();
            this.prbProgress = new System.Windows.Forms.ProgressBar();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblDesc2 = new System.Windows.Forms.Label();
            this.lblSourcePath = new System.Windows.Forms.Label();
            this.lblTargetPath = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnChooseSourcePath
            // 
            this.btnChooseSourcePath.Location = new System.Drawing.Point(316, 114);
            this.btnChooseSourcePath.Name = "btnChooseSourcePath";
            this.btnChooseSourcePath.Size = new System.Drawing.Size(21, 23);
            this.btnChooseSourcePath.TabIndex = 0;
            this.btnChooseSourcePath.Text = "...";
            this.btnChooseSourcePath.UseVisualStyleBackColor = true;
            this.btnChooseSourcePath.Click += new System.EventHandler(this.btnChooseSourcePath_Click);
            // 
            // btnChooseTargetPath
            // 
            this.btnChooseTargetPath.Location = new System.Drawing.Point(316, 143);
            this.btnChooseTargetPath.Name = "btnChooseTargetPath";
            this.btnChooseTargetPath.Size = new System.Drawing.Size(21, 23);
            this.btnChooseTargetPath.TabIndex = 1;
            this.btnChooseTargetPath.Text = "...";
            this.btnChooseTargetPath.UseVisualStyleBackColor = true;
            this.btnChooseTargetPath.Click += new System.EventHandler(this.btnChooseTargetPath_Click);
            // 
            // txtSourcePath
            // 
            this.txtSourcePath.Location = new System.Drawing.Point(137, 116);
            this.txtSourcePath.Name = "txtSourcePath";
            this.txtSourcePath.Size = new System.Drawing.Size(167, 20);
            this.txtSourcePath.TabIndex = 2;
            this.txtSourcePath.Tag = "";
            // 
            // txtTargetPath
            // 
            this.txtTargetPath.Location = new System.Drawing.Point(137, 145);
            this.txtTargetPath.Name = "txtTargetPath";
            this.txtTargetPath.Size = new System.Drawing.Size(167, 20);
            this.txtTargetPath.TabIndex = 3;
            // 
            // btnRun
            // 
            this.btnRun.Location = new System.Drawing.Point(264, 257);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(75, 23);
            this.btnRun.TabIndex = 4;
            this.btnRun.Text = "Convert";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // prbProgress
            // 
            this.prbProgress.Location = new System.Drawing.Point(31, 257);
            this.prbProgress.Name = "prbProgress";
            this.prbProgress.Size = new System.Drawing.Size(227, 23);
            this.prbProgress.TabIndex = 6;
            // 
            // txtStatus
            // 
            this.txtStatus.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtStatus.ForeColor = System.Drawing.Color.Red;
            this.txtStatus.Location = new System.Drawing.Point(31, 179);
            this.txtStatus.Multiline = true;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.Size = new System.Drawing.Size(308, 71);
            this.txtStatus.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(31, 18);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(308, 58);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Location = new System.Drawing.Point(110, 69);
            this.lblDescription.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(161, 17);
            this.lblDescription.TabIndex = 9;
            this.lblDescription.Text = "TimeSheet conversation";
            // 
            // lblDesc2
            // 
            this.lblDesc2.AutoSize = true;
            this.lblDesc2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesc2.Location = new System.Drawing.Point(110, 86);
            this.lblDesc2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDesc2.Name = "lblDesc2";
            this.lblDesc2.Size = new System.Drawing.Size(158, 17);
            this.lblDesc2.TabIndex = 10;
            this.lblDesc2.Text = "Pulseway PSA to MYOB";
            // 
            // lblSourcePath
            // 
            this.lblSourcePath.AutoSize = true;
            this.lblSourcePath.Location = new System.Drawing.Point(29, 118);
            this.lblSourcePath.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSourcePath.Name = "lblSourcePath";
            this.lblSourcePath.Size = new System.Drawing.Size(103, 13);
            this.lblSourcePath.TabIndex = 11;
            this.lblSourcePath.Text = "Source folder or files";
            // 
            // lblTargetPath
            // 
            this.lblTargetPath.AutoSize = true;
            this.lblTargetPath.Location = new System.Drawing.Point(29, 147);
            this.lblTargetPath.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTargetPath.Name = "lblTargetPath";
            this.lblTargetPath.Size = new System.Drawing.Size(92, 13);
            this.lblTargetPath.TabIndex = 12;
            this.lblTargetPath.Text = "Export to this path";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(374, 309);
            this.Controls.Add(this.lblTargetPath);
            this.Controls.Add(this.lblSourcePath);
            this.Controls.Add(this.lblDesc2);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.prbProgress);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.txtTargetPath);
            this.Controls.Add(this.txtSourcePath);
            this.Controls.Add(this.btnChooseTargetPath);
            this.Controls.Add(this.btnChooseSourcePath);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "CC Timesheet Converter version 1.1.0";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Button btnChooseSourcePath;
        protected System.Windows.Forms.Button btnChooseTargetPath;
        protected System.Windows.Forms.TextBox txtSourcePath;
        protected System.Windows.Forms.TextBox txtTargetPath;
        protected System.Windows.Forms.Button btnRun;
        protected System.Windows.Forms.ProgressBar prbProgress;
        protected System.Windows.Forms.TextBox txtStatus;
        protected System.Windows.Forms.PictureBox pictureBox1;
        protected System.Windows.Forms.Label lblDescription;
        protected System.Windows.Forms.Label lblDesc2;
        protected System.Windows.Forms.Label lblSourcePath;
        protected System.Windows.Forms.Label lblTargetPath;
    }
}

