﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CountryConsultingParser
{
    class FileStrategy : MainForm, ISourceValidator
    {
        public bool ValidateSources()
        {
            //Check file extensions
            foreach (String file in sourceFiles)
            {
                if (Path.GetExtension(file) != Constants.FileExtension)
                {
                    txtStatus.Text = Constants.InvalidExtension;
                    return false;
                }
            }
            return true;
        }
    }
}
