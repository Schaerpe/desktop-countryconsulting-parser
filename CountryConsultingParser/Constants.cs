﻿using System;

namespace CountryConsultingParser
{

    /// <summary>
    /// This class contains all constant String values used in the application
    /// </summary>
    class Constants
    {
        //User feedback
        public static String NoFiles = "No files could be found in the source path.";
        public static String InvalidSourcePath = "Sourcepath is not an existing folder.";
        public static String InvalidTargetPath = "Targetpath is not an existing folder.";
        public static String NoSourcePath = "Sourcepath is missing.";
        public static String NoTargetPath = "Targetpath is missing.";
        public static String InvalidExtension = "The files have the wrong file extension (should be .csv).";
        public static String Success = "Conversion successfully finished.";

        //Constants for conversion
        public static String StartOfFile = "Employee\tAccount\tProject/Ticket\tTitle\tEntry Date\tHours\tRole\tWork Type\tBillable\tNotes";
        public static String TotalKeyword = "Total:";
        public static String FileExtension = ".csv";
    }

}
