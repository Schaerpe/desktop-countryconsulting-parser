﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CountryConsultingParser
{
    /// <summary>
    /// Helper class that provides methods to format the textfiles
    /// </summary>
    internal class ConvertionHelper
    {
        /// <summary>
        /// Removes all quotation marks and splits the fields by tabs instead of commas
        /// </summary>
        /// <param name="str">The text to be formatted</param>
        /// <returns>The formatted text</returns>
        public static String[] SplitFieldsByTabs(String[] lines)
        {
            for (int i = 0; i < lines.Length; i++) 
            {
                lines[i] = lines[i].Replace("\",\"", "\t");
                lines[i] = lines[i].Replace("\"", "");
            }
            return lines;
        }

        /// <summary>
        /// Cuts off the unnecessary part in the beginning of the textfile
        /// </summary>
        /// <remarks>
        /// <para>Matches the text of the table headers and cuts everything above them</para>
        /// </remarks>
        /// <param name="str">The text to be formatted</param>
        /// <returns>The formatted text</returns>
        public static String[] CutHead(String[] lines)
        {
            List<string> result = new List<string>(lines);
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains(Constants.StartOfFile))
                {
                    result.RemoveRange(0, i);
                }
            }
            return result.ToArray();
        }

        /// <summary>
        /// Separates the name field into first- and lastname, renames the column headers
        /// </summary>
        /// <remarks>
        /// <para>Changes the column header names in the first line, then replaces all blanks by tabs in the following lines</para>
        /// </remarks>
        /// <param name="str">The text to be formatted</param>
        /// <returns>A string array where every index points to a line of the formatted text</returns>
        public static String[] UpdateColumnNames(String[] lines)
        {
            //split first- and lastname
            for (int i = 0; i < lines.Length; i++)
            {
                if (i <= 0)
                {
                    lines[0] = lines[0].Replace("Employee", "Employee First Name\tEmployee Last Name");
                }
                else
                {
                    int indexOfFirstBlank = lines[i].IndexOf(" ");
                    if (indexOfFirstBlank > 0)
                    {
                        StringBuilder sb = new StringBuilder(lines[i]);
                        sb.Remove(indexOfFirstBlank, 1);
                        sb.Insert(indexOfFirstBlank, "\t");
                        lines[i] = sb.ToString();
                    }
                }
            }

            //rename columns
            lines[0] = lines[0].Replace("Hours", "Units");
            lines[0] = lines[0].Replace("Account", "Cust. Co./Last Name");
            lines[0] = lines[0].Replace("Role", "Activity ID");
            lines[0] = lines[0].Replace("Entry Date", "Date");
            lines[0] = lines[0].Replace("Employee First Name", "Emp. First Name");
            lines[0] = lines[0].Replace("Employee Last Name", "Emp. Co./Last Name");

            return lines;
        }

        /// <summary>
        /// Splits string up into a string array, each index pointing to a line of text
        /// </summary>
        /// <param name="contents">The string to be split up</param>
        /// <returns>The resulting string array</returns>
        public static String[] SplitLines(String contents)
        {
            return contents.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
        }

        /// <summary>
        /// Removes all the unnecessary lines containing the keyword "Total"
        /// </summary>
        /// <param name="lines">String array where every index points to a line of the text</param>
        /// <returns>The string array without the lines containing "Total"</returns>
        public static String[] RemoveTotals(String[] lines)
        {
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains(Constants.TotalKeyword))
                {
                    List<String> lineList = new List<String>(lines);
                    lineList.RemoveAt(i);
                    lines = lineList.ToArray();
                }
            }
            return lines;
        }

        /// <summary>
        /// Changes the dage from american format (m/d/y) to english (d/m/y)
        /// </summary>
        /// <param name="lines">String array where every index points to a line of the text</param>
        /// <returns>The string array with correct date format</returns>
        public static String[] ReformatDate(String[] lines)
        {
            int dateIndex = 0;
            for (int i = 0; i < lines.Length; i++)
            {
                String[] parts = lines[i].Split('\t');
                if (i == 0)
                {
                    //if it is header line, find the index of the date
                    for (int j = 0; j < parts.Length; j++)
                    {
                        if (parts[j].Equals("Date"))
                        {
                            dateIndex = j;
                            break;
                        }
                    }
                }
                else
                {
                    //format date in other lines
                    if (parts.Length > dateIndex)
                    {
                        if (parts[dateIndex].Contains('/'))
                        {
                            String[] dateArr = parts[dateIndex].Split('/');
                            parts[dateIndex] = String.Concat(dateArr[1], '/', dateArr[0], '/', dateArr[2]);
                        }
                    }

                    //reassemble line
                    string line = "";
                    for (int j = 0; j < parts.Length; j++)
                    {
                        if (j > 0)
                        {
                            line += "\t";
                        }
                        line += parts[j];
                    }
                    lines[i] = line;
                    Console.WriteLine(i);
                }
            }
            return lines;
        }

    }
}