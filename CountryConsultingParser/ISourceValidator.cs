﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountryConsultingParser
{
    /// <summary>
    /// Interface used for Strategy-Pattern to enable the user to either selected files or enter a folder path
    /// </summary>
    interface ISourceValidator
    {
    
        /// <summary>
        /// Validates the source files to be converted
        /// </summary>
        /// <returns>true if all is good</returns>
        bool ValidateSources();

    }
}
